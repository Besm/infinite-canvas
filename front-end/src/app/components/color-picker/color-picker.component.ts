import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSlider, ThemePalette } from '@angular/material';
import { ColorConverter } from './ColorConverter';
import { Vector } from 'src/internal-logic/Vector';
import { Color } from 'src/internal-logic/Color';




@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css']
})
export class  ColorPickerComponent implements OnInit {

  constructor() { }
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('colorShower') colorShower: ElementRef;
  colors: String[];
  imageData: ImageData;
  width: number;
  ctx: CanvasRenderingContext2D;
  hueSliderHeight: number = 30;

  hueSliderX: number = 0;
  colorPickerPos: Vector = new Vector(0,0);
  isClicking : boolean;
  public static currentColor : Color;

  public getColor(){
    return ColorPickerComponent.currentColor;
  }

  regenerateImage(){
    let height: number = this.width - this.hueSliderHeight;
    let hue = this.hueSliderX / this.width;

    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < height; y++) {


        let c = y * (this.width * 4) + x * 4;
        let rgb = ColorConverter.hsvToRgb(hue, (1 - y / height), x / this.width);
        this.imageData.data[c + 0] = rgb[0];
        this.imageData.data[c + 1] = rgb[1];
        this.imageData.data[c + 2] = rgb[2];
        this.imageData.data[c + 3] = 255;

      }
    }

    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.hueSliderHeight; y++) {

        let real_x = x;
        let real_y = height + y;
        let c = real_y * (this.width * 4) + real_x * 4;
        let rgb = ColorConverter.hsvToRgb(x / this.width, 1, 1);
        this.imageData.data[c + 0] = rgb[0];
        this.imageData.data[c + 1] = rgb[1];
        this.imageData.data[c + 2] = rgb[2];
        this.imageData.data[c + 3] = 255;
      }
    }
  }

  draw(){
    this.ctx.clearRect(0, 0, this.width, this.width);
    this.ctx.putImageData(this.imageData, 0, 0);
    this.drawPickers();
  }
  drawPickers() {

    let height: number = this.width - this.hueSliderHeight;

    this.ctx.fillStyle = 'black'
    this.ctx.fillRect(this.hueSliderX - 1, height, 1, this.hueSliderHeight);

    // let chosenColor = ColorConverter

    this.ctx.beginPath();
    let p = this.colorPickerPos.y * (this.width * 4) + this.colorPickerPos.x * 4;
    let hsv = ColorConverter.rgbToHsv( this.imageData.data[p],this.imageData.data[p+1],this.imageData.data[p+2]);
    
    ColorPickerComponent.currentColor = new Color(this.imageData.data[p],this.imageData.data[p+1],this.imageData.data[p+2],255);

    let rgb = ColorConverter.hsvToRgb(hsv[0],hsv[1],hsv[2]);
    this.colorShower.nativeElement.style.backgroundColor = 'rgb('+rgb[0]+","+rgb[1]+","+rgb[2] + ")";

    this.ctx.strokeStyle= 'white';
    this.ctx.lineWidth = 10;
    this.ctx.arc(this.colorPickerPos.x,this.colorPickerPos.y,1,0,2*Math.PI);
    this.ctx.stroke();
    this.ctx.closePath();
    
  }

  onMouseUp(e : MouseEvent){
    this.isClicking = false;
  }
  onMouseDown(e : MouseEvent){
    this.isClicking = true;
  }

  onMouseClick(e: MouseEvent) {
    this.update(e);

  }
  onMouseMove(e: MouseEvent) {
    if(!this.isClicking){
      return;
    }
    this.update(e);

  }


  private update(e: MouseEvent) {
    let target = e.target as HTMLTextAreaElement;
    let x = e.x - target.getBoundingClientRect().left;
    let y = e.y - target.getBoundingClientRect().top;
    // console.log(new Vector(x, y));
    if (y > this.width - this.hueSliderHeight-1) {
      this.hueSliderX = x;
      this.regenerateImage();
    }
    else {
      this.colorPickerPos = new Vector(x, y);

    }
    this.draw();
  }

  ngOnInit() {

    this.colors = [];
    this.colors.push("red");
    this.colors.push("green");
    this.colors.push("black");
    this.colors.push("white");
    this.colors.push("yellow");

    this.width = this.canvas.nativeElement.width;
    // this.hueSliderHeight =  Math.round(this.width / 7) ;
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.imageData = this.ctx.createImageData(this.width, this.width);

    for (let i = 0; i < 20; i++) {

      this.colors.push("rgb(" + Math.round(Math.random() * 255) + "," + Math.round(Math.random() * 255) + "," + Math.round(Math.random() * 255) + ")");

    }

    this.hueSliderX = 0;
    this.colorPickerPos = new Vector(0,0);
    this.regenerateImage();
    this.draw();

    // document.addEventListener("mousemove",()=>this.redrawPicker());




  }



}
