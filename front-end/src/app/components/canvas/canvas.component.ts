import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Vector } from '../../../internal-logic/Vector';
import { InputSystem } from '../../../internal-logic/Input';
import { InfiniteCanvas } from 'src/internal-logic/InfiniteCanvas';
import { Container } from 'pixi.js'
import { DragDropModule } from '@angular/cdk/drag-drop';
import { Element } from '@angular/compiler';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Line } from 'src/internal-logic/Line';
import { RestService } from '../../services/rest/rest.service';


declare var PIXI : any;

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {



  constructor(private rest: RestService) { }
  @ViewChild('pixiContainer') pixiContainer: any; // this allows us to reference and load stuff into the div container

  public app: PIXI.Application; // this will be our pixi application
  private i: number = 0;
  private lines: Vector[] = [];
  private infiniteCanvas: InfiniteCanvas;


  ngOnInit() {

    var width = this.pixiContainer.nativeElement.clientWidth;
    var height = this.pixiContainer.nativeElement.clientHeight;
    let lines: Line[];
    this.app = new PIXI.Application({ width: width, height: height, antialias: true }); // this creates our pixi application
    
    this.app.renderer.view.onmousedown = (ev) => {
    };

    

    this.pixiContainer.nativeElement.appendChild(this.app.view); // this places our pixi application onto the viewable document

    this.app.renderer.backgroundColor = 0xffffff
    PIXI.ticker.shared.autoStart = false; PIXI.ticker.shared.stop();
    this.app.ticker.add((deltatime: 23.1) => this.animate());

    
    this.infiniteCanvas = new InfiniteCanvas(this.app,this.rest.http);

  }



  private animate() {
    //this.app.renderer.render(this.app.stage);
  }
  private draw(graphics: PIXI.Graphics) {
    var pos = this.app.renderer.plugins.interaction.mouse.global;



  }


}
