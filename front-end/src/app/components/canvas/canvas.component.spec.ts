import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasComponent } from './canvas.component';
import { DragableComponent } from '../dragable/dragable.component';
import { ColorPickerComponent } from '../color-picker/color-picker.component';
import { MatSlider } from '@angular/material';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('CanvasComponent', () => {
  let component: CanvasComponent;
  let fixture: ComponentFixture<CanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [
         CanvasComponent,
        DragableComponent,
        ColorPickerComponent,
        MatSlider ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
