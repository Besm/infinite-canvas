import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CanvasComponent } from '../canvas/canvas.component';
import { DragableComponent } from '../dragable/dragable.component';
import { ColorPickerComponent } from '../color-picker/color-picker.component';
import { HttpClientModule } from '@angular/common/http'; 

import { MatSlider } from '@angular/material';
import { HttpClientTestingModule } from '@angular/common/http/testing';
declare var PIXI : any;
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [HttpClientModule],
      imports: [
        HttpClientTestingModule,
    ],

      declarations: [
        AppComponent,
        CanvasComponent,
        DragableComponent,
        ColorPickerComponent,
        MatSlider
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });


  it('should render a canvas component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    console.log(compiled.querySelector('app-canvas'));
    
    expect(compiled.querySelector('app-canvas')).not.toBeNull();
  });
});
