import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CanvasComponent } from '../canvas/canvas.component';
import { DragableComponent } from '../dragable/dragable.component';
import {DemoMaterialModule} from '../../../material-module';
import {iro} from '@jaames/iro';
import { ColorPickerComponent } from '../color-picker/color-picker.component'
import { HttpClientModule } from '@angular/common/http'; 



@NgModule({
  declarations: [
    AppComponent,
    CanvasComponent,
    DragableComponent,
    ColorPickerComponent,
  ],
  imports: [
    BrowserModule,
    DemoMaterialModule,  
    HttpClientModule  
  ],
  exports:[
    DragableComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
