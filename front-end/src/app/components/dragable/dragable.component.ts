import { Component, OnInit, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { MatSlider } from '@angular/material';
import { Input } from '@angular/compiler/src/core';
import { listener } from '@angular/core/src/render3';

@Component({
  selector: 'app-dragable',
  templateUrl: './dragable.component.html',
  styleUrls: ['./dragable.component.css']
})
export class DragableComponent implements OnInit {

  brushSize: number;
  brushColor: string;

  @ViewChild('brushSizeSlider')
  brushSizeSlider: MatSlider;

  @ViewChild('pageInput')
  pageInput: ElementRef;

  public static brushWidth: number;
  public static onPageSwitch: EventEmitter<String>;

  constructor() {
    DragableComponent.onPageSwitch = new EventEmitter<String>();

  }

  pageSwitch() {
    DragableComponent.onPageSwitch.emit(this.pageInput.nativeElement.value);
  }

  ngOnInit() {
    this.brushSizeSlider.registerOnChange((v) => DragableComponent.brushWidth = v);
    this.brushSizeSlider.input.subscribe((e: any) => {
      this.brushSize = +this.brushSizeSlider.displayValue
    });
    this.brushSizeSlider.value = 5.00;
    this.brushSize = this.brushSizeSlider.value;
    DragableComponent.brushWidth = this.brushSize;

  }



}
