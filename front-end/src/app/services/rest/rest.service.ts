import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Line } from 'src/internal-logic/Line';
import { ServerResponse } from 'src/internal-logic/RestSystem';


@Injectable({
  providedIn: 'root',
})
export class RestService {

  constructor(public http: HttpClient) { }

  public get(path) : Observable<Object> {
    return this.http.get("http://localhost/pages/test/lines");   
  }

 
}
