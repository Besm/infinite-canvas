import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {DragDropModule} from '@angular/cdk/drag-drop';

import { AppModule } from './app/components/app-component/app.module';
import { environment } from './environments/environment';
import {DemoMaterialModule} from './material-module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
