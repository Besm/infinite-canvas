import { CanvasSystem } from "./CanvasSystem";
import { LineHolderSystem } from './LineHolderSystem';
import { Line } from './Line';
import { Vector } from './Vector';
import { LineDrawerSystem } from './LineDrawerSystem';
import { Mesh, Point } from 'pixi.js';
import { ColorPickerComponent } from 'src/app/components/color-picker/color-picker.component';
import { Color } from './Color';


enum LineType {
    Point,
    FullLine
}

export class LineRendererSystem extends CanvasSystem {
    lineholder: LineHolderSystem;
    drawerSystem: LineDrawerSystem;
    cachedLines: Map<number, Mesh>;
    points: number[];
    triangles: number[];
    mainShader: PIXI.Shader;
    currentdrawingMesh: Mesh;
    vertShader: string;
    fragShader: string;

    start(): void {
        this.lineholder = this.infinitecanvas.getSystem(LineHolderSystem);
        this.drawerSystem = this.infinitecanvas.getSystem(LineDrawerSystem);
        this.cachedLines = new Map();
        this.vertShader = `
        precision mediump float;
        attribute vec2 aVertexPosition;
    
        uniform mat3 translationMatrix;
        uniform mat3 projectionMatrix;
    
        uniform vec3 color;

        void main() {
    
            gl_Position = vec4((projectionMatrix * translationMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
            
        }`;
        this.fragShader = `precision mediump float;
    
        uniform vec3 color;

    
        void main() {
            gl_FragColor = vec4(color, 1.0);
        }
    
    `;
        this.mainShader = PIXI.Shader.from(this.vertShader, this.fragShader);
        this.lineholder.newlineEvent.subscribe((line: Line) => {
            if (line.points.length != 0) {
                let mesh = this.generateMesh(line);
                this.infinitecanvas.pixiApp.stage.addChild(mesh);

                console.log("done");
            }
        })

        this.lineholder.clearEvent.subscribe(() => {
            this.infinitecanvas.pixiApp.stage.removeChildren();
        })
    }
    fixedUpdate(): void {
    }


    private generateLine(line: Line): Vector[] {


        var drawpoints: Vector[];
        drawpoints = [];

        var firstPoints: Vector[] = [];
        let lineType = LineType.FullLine;


        if (line.points.length <= 1) {
            lineType = LineType.Point;
        }

        if (lineType == LineType.FullLine) {
            for (let i = 0; i < line.points.length; i++) {
                const point = line.points[i];

                var dir;
                if (i == 0) {
                    let dirToNext = Vector.normalize(Vector.minus(line.points[i + 1], point))
                    dir = dirToNext;
                } else if (i == line.points.length - 1) {
                    let dirToLast = Vector.normalize(Vector.minus(point, line.points[i - 1]))
                    dir = dirToLast;
                } else {
                    let dirToLast = Vector.normalize(Vector.minus(point, line.points[i - 1]))
                    let dirToNext = Vector.normalize(Vector.minus(line.points[i + 1], point))
                    dir = Vector.divide(Vector.plus(dirToNext, dirToLast), 2);
                }
                // let dir = dirToLast;
                let perpindiculer = new Vector(-dir.y, dir.x);
                firstPoints.push(Vector.plus(point, Vector.multiply(perpindiculer, line.width)));
                firstPoints.push(Vector.minus(point, Vector.multiply(perpindiculer, line.width)));
            }
        }
        return firstPoints;
    }
    
    private generateMesh(line: Line): Mesh {
        var rawPoints = this.generateLine(line);

        var points = [];
        for (let i = 0; i < rawPoints.length; i++) {
            points.push(rawPoints[i].x);
            points.push(rawPoints[i].y);

        }

        var geometry = new PIXI.Geometry()
            .addAttribute('aVertexPosition', PIXI.Buffer.from(points))
        //.addAttribute('color', PIXI.Buffer.from([1,0,1]));


        var s = line.color.replace('rgb(', '').replace(')', '').split(',');
        var c: number[] = [];
        for (let i = 0; i < s.length; i++) {
            c[i] = Number.parseInt(s[i]) / 255;
        }
        var col: Color = new Color(c[0], c[1], c[2]);
        var mesh: PIXI.Mesh = new PIXI.Mesh(geometry, PIXI.Shader.from(this.vertShader, this.fragShader, { color: [col.R, col.G, col.B] }));
        mesh.drawMode = 5;
        return mesh;
    }

    draw(): void {

        this.infinitecanvas.pixiApp.stage.removeChild(this.currentdrawingMesh);

        if (this.drawerSystem.currentPoints.length > 0) {
            this.currentdrawingMesh = this.generateMesh(new Line(this.drawerSystem.currentPoints, this.drawerSystem.thickness, ColorPickerComponent.currentColor.toRGB()));
            this.infinitecanvas.pixiApp.stage.addChild(this.currentdrawingMesh);
        }
    }
}
