import { InfiniteCanvas } from "./InfiniteCanvas";

export abstract class CanvasSystem {
    infinitecanvas: InfiniteCanvas;
    constructor(infiniteCanvas: InfiniteCanvas) {
        this.infinitecanvas = infiniteCanvas;
    }
    abstract start(): void;
    abstract fixedUpdate(): void;
    abstract draw(): void;
}
