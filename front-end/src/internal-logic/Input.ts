import { Button } from "./MouseButton";
import { CanvasSystem } from "./CanvasSystem";
import { Key } from './Key';
import { Vector } from './Vector';
import { Color } from './Color';

export class InputSystem extends CanvasSystem {

  private buttonPressed: Set<Button>;
  private buttonGotPressedLastFrame: Set<Button>;
  private buttonGotReleasedLastFrame: Set<Button>;

  private keyPressed: Set<Key>;
  private keyGotPressedLastFrame: Set<Key>;
  private keyGotReleasedLastFrame: Set<Key>;

  private realMousePos: Vector;
  private expectedMousePos: Vector;
  private currentScroll : number =0;
  private smoothScroll : number =0;
 
  private colorPickerColor : Color;
 
  start(): void {

    this.realMousePos = new Vector(0,0);
    this.keyPressed = new Set();
    this.keyGotPressedLastFrame = new Set();
    this.keyGotReleasedLastFrame = new Set();
    this.buttonPressed = new Set();
    this.buttonGotPressedLastFrame = new Set();
    this.buttonGotReleasedLastFrame = new Set();

    let mainCanvas = document.getElementsByTagName('canvas')[0];
    mainCanvas.addEventListener("keyup", (ev: KeyboardEvent) => this.keyUp(ev.which))
    
    mainCanvas.addEventListener("keydown", (ev: KeyboardEvent) => {
      if (!ev.repeat)
        this.keyDown(ev.which)
    })
    mainCanvas.addEventListener("mousedown", (ev: MouseEvent) => this.mouseDown(ev.which))
    mainCanvas.addEventListener("mouseup", (ev: MouseEvent) => this.mouseUp(ev.which))
    
    mainCanvas.addEventListener("mousemove", (ev: MouseEvent) => {
      // this.expectedMousePos = new Vector(ev.x,ev.y);
    });
    mainCanvas.addEventListener("mouseleave",()=>{
      this.buttonPressed.clear();
    })
    mainCanvas.addEventListener("wheel", (ev: WheelEvent) => {
      if(ev.deltaY>=0){
        this.currentScroll++;
      } else {
        this.currentScroll--;
      }      
    });

    let colorPickerCanvas = document.getElementsByTagName('canvas')[1];

    colorPickerCanvas.addEventListener("mouseup",(ev : MouseEvent) => {
      
    });
    

    //WindowBase.MouseDown += Mouse_ButtonDown;
    //WindowBase.MouseUp += Mouse_ButtonUp;
    // WindowBase.MouseMove += Mouse_Move;  
  }
  private mouseDown(which: Button): any {
    //console.log("mouse pressed " + Button[which]);
    this.buttonPressed.add(which);
    this.buttonGotPressedLastFrame.add(which);
  }

  private mouseUp(which: Button): any {
   // console.log("mouse pressed " + Button[which]);
    this.buttonPressed.delete(which);
    this.buttonGotReleasedLastFrame.add(which);
  }

  private keyDown(key: Key): any {

    //console.log("key pressed " + Key[key]);
    this.keyPressed.add(key);
    this.keyGotPressedLastFrame.add(key);
  }

  private keyUp(key: Key): any {
   // console.log("key let go");
    this.keyPressed.delete(key);
    this.keyGotReleasedLastFrame.add(key);
  }

  public isKeyPressed(key: Key): boolean {
    return this.keyGotPressedLastFrame.has(key);
  }
  public isKeyReleased(key: Key): boolean {
    return this.keyGotReleasedLastFrame.has(key);
  }

  public isKeyHeld(key: Key): boolean {
    return this.keyPressed.has(key);
  }
  public isButtonPressed(button: Button): boolean {
    return this.buttonGotPressedLastFrame.has(button);
  }
  public isButtonHeld(button: Button): boolean {
    return this.buttonPressed.has(button);
  }
  public isButtonReleased(button: Button): boolean {
    return this.buttonGotReleasedLastFrame.has(button);
  }

  public getScroll() : number{
    return this.currentScroll;
  }

  public getSmoothScroll() : number{
    return this.smoothScroll;
  }
  public getMousePosistion(): Vector {
    return this.realMousePos.clone();
  }

  public getColorPickerColor()  : Color{
    return this.colorPickerColor.clone();
  }

  public fixedUpdate(): void {

  }

  public draw(): void {
    this.refresh();

  }

  



  public refresh() {

    this.buttonPressed.forEach(element => {
      if(!this.isButtonHeld(element)){
        this.buttonPressed.delete(element);
      }
    });

    let newest = this.infinitecanvas.pixiApp.renderer.plugins.interaction.mouse.global;
  
     
    this.realMousePos = new Vector(newest.x,newest.y);
    let c = .5;
    this.smoothScroll = c * this.smoothScroll + (1.0 - c) * this.currentScroll;
    this.currentScroll = 0;

    this.keyGotPressedLastFrame.clear();
    this.keyGotReleasedLastFrame.clear();
    this.buttonGotReleasedLastFrame.clear();
    this.buttonGotPressedLastFrame.clear();
  }
}
