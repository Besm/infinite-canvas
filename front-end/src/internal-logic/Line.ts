import { Vector } from './Vector';
export class Line {
    id: number;
    points: Vector[];
    width: number;
    color: string;

    constructor(points: Vector[], width: number, color: string) {
        this.points = points;
        this.width = width;
        this.color = color;
    }
}
