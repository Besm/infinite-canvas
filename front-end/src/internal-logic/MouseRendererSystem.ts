import { CanvasSystem } from "./CanvasSystem";
import { InputSystem } from './Input';

export class MouseRendererSystem extends CanvasSystem {
   
    input: InputSystem;
    start(): void {
        this.input = this.infinitecanvas.getSystem(InputSystem);
    }
    fixedUpdate(): void {
    }

    draw(): void {
        var mousepos = this.input.getMousePosistion();
        let graphics = new PIXI.Graphics();
        if (mousepos != null)
            graphics.lineStyle(0, 0x000000);
        graphics.beginFill(0x000000);
        graphics.drawCircle(mousepos.x, mousepos.y, 5);
        graphics.endFill();
    }
}
