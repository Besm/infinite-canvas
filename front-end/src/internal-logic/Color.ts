export class Color {
    public R: number;
    public G: number;
    public B: number;
    public A: number;
    
    constructor(r: number, g: number, b: number, a: number = 255) {
        this.R = r;
        this.G = g;
        this.B = b;
        this.A = a;
    }

    public toRGB() : string {
        return `rgb(${this.R},${this.G},${this.B})`;
    } 

    public clone() : Color{
        return new Color(this.R,this.G,this.B,this.A);
    }
    
  
}
