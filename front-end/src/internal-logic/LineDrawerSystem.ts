import { CanvasSystem } from "./CanvasSystem";
import { InputSystem } from './Input';
import { Vector } from './Vector';
import { Button } from './MouseButton';
import { LineHolderSystem } from './LineHolderSystem';
import { Line } from './Line';
import { Key } from './Key';
import { Point } from 'pixi.js';
import { ColorPickerComponent } from 'src/app/components/color-picker/color-picker.component';
import { DragableComponent } from 'src/app/components/dragable/dragable.component';


export class LineDrawerSystem extends CanvasSystem {
    input: InputSystem;
    lineholder: LineHolderSystem;
    currentPoints: Vector[];

    thickness: number;

    start(): void {
        this.input = this.infinitecanvas.getSystem(InputSystem);
        this.lineholder = this.infinitecanvas.getSystem(LineHolderSystem);
        this.currentPoints = [];
    }

    fixedUpdate(): void {

        let scale = this.infinitecanvas.pixiApp.stage.scale.x;
        this.thickness = DragableComponent.brushWidth / scale; 

        if(this.input.isKeyHeld(Key.Space)){
            let screen_width = this.infinitecanvas.pixiApp.stage.width;
            let screen_height = this.infinitecanvas.pixiApp.stage.height;
            
            let s = this.infinitecanvas.pixiApp.stage.scale ;
            this.infinitecanvas.pixiApp.stage.pivot = new Point(0,0);
            this.infinitecanvas.pixiApp.stage.position.x -= this.input.getMousePosistion().x/10 * s.x;
            this.infinitecanvas.pixiApp.stage.position.y -= this.input.getMousePosistion().y/10 * s.y;
            this.infinitecanvas.pixiApp.stage.scale = new Point(s.x* 1.1,s.y* 1.1) ; 
        }
    }

    private worldSpaceMousePos() : Vector {
        var mousepos = new Point(this.input.getMousePosistion().x, this.input.getMousePosistion().y);
        var newpos = new Point();
        this.infinitecanvas.pixiApp.stage.worldTransform.applyInverse(mousepos, newpos);
        //console.log(mousepos);
        //console.log(this.infinitecanvas.pixiApp.stage.transform.localTransform.invert().apply(mousepos, newpos));

        return Vector.fromPoint(newpos);
    }

    draw(): void {

        if (this.input.isButtonHeld(Button.Left)) {
            let pos = this.worldSpaceMousePos();
                
            if (this.currentPoints.length == 0) {
                this.currentPoints.push( pos);
            } else {
                var lastpos = this.currentPoints[this.currentPoints.length - 1]
           
                var dis = Vector.distance(pos, lastpos);
                if (dis > 0) {
                    this.currentPoints.push(pos);
                }

            }
        }
        if (this.input.isButtonReleased(Button.Left)) {
       
            this.lineholder.addLine(new Line(this.currentPoints, this.thickness,ColorPickerComponent.currentColor.toRGB()));
            this.currentPoints = [];
        }


    }



}


