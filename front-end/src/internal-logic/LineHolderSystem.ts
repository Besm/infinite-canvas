import { CanvasSystem } from "./CanvasSystem";
import { Line } from "./Line";
import { EventEmitter } from '@angular/core';
import { RestSystem, ServerResponse } from './RestSystem';
import { DragableComponent } from 'src/app/components/dragable/dragable.component';



export class LineHolderSystem extends CanvasSystem {

    lines: Line[];
    newlineEvent: EventEmitter<Line> = new EventEmitter<Line>();
    clearEvent: EventEmitter<Boolean> = new EventEmitter();

    restSystem: RestSystem;

    pageName;
    start(): void {
        this.lines = [];
        this.restSystem = this.infinitecanvas.getSystem(RestSystem);
        this.pageName = "";
        this.updateLineData(this.pageName);
        this.clearEvent = new EventEmitter<Boolean>();
        DragableComponent.onPageSwitch.subscribe((page) => {
            this.clearEvent.emit();
            this.lines = [];
            this.pageName = page;
            this.updateLineData(page);

        })
    }
    private updateLineData(pageName: String) {
        this.restSystem.getLines(pageName).subscribe((data: ServerResponse<Line[]>) => {
            var response: ServerResponse<Line[]> = data;
            var lines: Line[] = response.result;
            console.log(response);
            for (let i = 0; i < lines.length; i++) {
                const line = lines[i];
                this.lines.push(line);
                this.newlineEvent.emit(line);
            }
        });
    }

    fixedUpdate(): void {

    }
    draw(): void {
    }

    public addLine(line: Line) {

        for (let i = 0; i < line.points.length; i++) {
            line.points[i].x = Math.round(line.points[i].x * 100) / 100;
            line.points[i].y = Math.round(line.points[i].y * 100) / 100;

        }
        this.lines.push(line);

        this.newlineEvent.emit(line);
    }
}
