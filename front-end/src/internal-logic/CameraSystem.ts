import { CanvasSystem } from "./CanvasSystem";
import { InputSystem } from './Input';
import { Point, Rectangle } from 'pixi.js';
import { Vector } from './Vector';
import { Button } from './MouseButton';
export class CameraSystem extends CanvasSystem {
    input: InputSystem;
    mouseMoveStartPos: Vector;
    worldStartPos: Vector;

    lastMousePos : Vector;

    start(): void {
        this.input = this.infinitecanvas.getSystem(InputSystem);
        let screen = this.infinitecanvas.pixiApp.screen;
        // this.infinitecanvas.pixiApp.stage.scale.x = 10;
        // this.infinitecanvas.pixiApp.stage.scale.y = 10;
    }
    fixedUpdate(): void {
    }
    private worldSpaceMousePos() : Vector {
        var mousepos = new Point(this.input.getMousePosistion().x, this.input.getMousePosistion().y);
        var newpos = new Point();
        this.infinitecanvas.pixiApp.stage.worldTransform.applyInverse(mousepos, newpos);
        //console.log(mousepos);
        //console.log(this.infinitecanvas.pixiApp.stage.transform.localTransform.invert().apply(mousepos, newpos));

        return Vector.fromPoint(newpos);
    }


    private toWorldSpace(v : Vector) : Vector {
        var newpos = new Point();
        this.infinitecanvas.pixiApp.stage.worldTransform.applyInverse(v.toPoint(), newpos);
        return Vector.fromPoint(newpos);
    }
    draw(): void {
        //    console.log(this.worldSpaceMousePos());

            if(!this.input.isButtonHeld(Button.Middle)){
            this.checkScroll();
            }
            this.checkMiddleMove();

    }

    private checkMiddleMove() {
        if (this.input.isButtonPressed(Button.Middle)) {
            this.mouseMoveStartPos =  this.input.getMousePosistion();
            this.worldStartPos = Vector.fromPoint(this.infinitecanvas.pixiApp.stage.position);
        }
        if (this.input.isButtonHeld(Button.Middle)) {

          //  var v = Vector.minus(this.worldStartPos, Vector.minus(this.mouseMoveStartPos, this.worldSpaceMousePos()));
            var diffScreenSpace = Vector.minus(this.mouseMoveStartPos, this.input.getMousePosistion());
            let scale = this.infinitecanvas.pixiApp.stage.scale.x;
            var diffWorldSpace = Vector.minus(this.toWorldSpace(this.mouseMoveStartPos),this.toWorldSpace(this.input.getMousePosistion())) 
            var muliplied = Vector.multiply(diffWorldSpace,scale);
            var pos = Vector.minus(this.worldStartPos,muliplied);           
            
            this.infinitecanvas.pixiApp.stage.position.x = pos.x;
            this.infinitecanvas.pixiApp.stage.position.y = pos.y;
        }
    }

    private checkScroll() {
        let screen = this.infinitecanvas.pixiApp.screen;
        let scalar = 1 - (this.input.getScroll() / 4);
        // camera.x += screen.width / 2;
        // camera.y += screen.height / 2;
        let diff = this.infinitecanvas.pixiApp.stage.scale.x;
        this.infinitecanvas.pixiApp.stage.scale.x *= scalar;
        this.infinitecanvas.pixiApp.stage.scale.y *= scalar;
        diff -= this.infinitecanvas.pixiApp.stage.scale.x;
        this.infinitecanvas.pixiApp.stage.position.x -= screen.width / 2 * -diff;
        this.infinitecanvas.pixiApp.stage.position.y -= screen.height / 2 * -diff;
        let center = new Vector(0, 0);
        center.x = screen.width / 2;
        center.y = screen.height / 2;
        let delta = Vector.minus(this.worldSpaceMousePos(), center);
        this.infinitecanvas.pixiApp.stage.position.x += delta.x * diff;
        this.infinitecanvas.pixiApp.stage.position.y += delta.y * diff;
    }
}
