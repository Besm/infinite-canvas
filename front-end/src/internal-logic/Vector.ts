import { Point } from 'pixi.js';

export class Vector {
  public x: number;
  public y: number;
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  public clone(): Vector {
    return new Vector(this.x,this.y);
  }

  public toPoint() : Point {
    return new Point(this.x,this.y);
  }

  public static fromPoint (p : Point ){
    return new Vector(p.x,p.y);
  }
  public static minus(a: Vector, b: Vector) {
    return new Vector(a.x - b.x, a.y - b.y);
  }
  public static plus(a: Vector, b: Vector) {
    return new Vector(a.x + b.x, a.y + b.y);
  }

  public toString() : String{
    return "X="+this.x.toString() + " Y="+this.y.toString();
  }
  
  public static multiply(a: Vector, b: number) : Vector{
    return new Vector(a.x * b, a.y * b);
  }
  public static divide(a: Vector, b: number) : Vector {
    return new Vector(a.x / b, a.y / b);
  }
  public static vectorLength(a: Vector) {
    return Math.sqrt(a.x * a.x + a.y * a.y);
  }

  public static normalize(a: Vector): Vector {
    var length = this.vectorLength(a);
    return new Vector(a.x / length, a.y / length);
  }

  public static distance(a: Vector, b: Vector): number {
    return Vector.vectorLength(Vector.minus(b, a));
  }

  
}


