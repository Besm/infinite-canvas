import { CanvasSystem } from "./CanvasSystem";
export class RendererSystem extends CanvasSystem {
    start(): void {
    }
    fixedUpdate(): void {
    }
    draw(): void {
    }
}
