import { CanvasSystem } from "./CanvasSystem";
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Line } from './Line';
import { post } from 'selenium-webdriver/http';

export class ServerResponse<T> {
    succes: Boolean;
    result: T;
}
  

export class RestSystem extends CanvasSystem {
    
    private http: HttpClient;
    private serverPath : string;
      
    private httpOptions : any = {
        headers: new HttpHeaders({ 
          'Access-Control-Allow-Origin':'*',
          'Authorization':'authkey',
          'userid':'1'
        })
      };
    
    start(): void {
        this.http = this.infinitecanvas.http;
        //this.serverPath = "http://localhost";
        this.serverPath = "https://infinitecanvasapi.azurewebsites.net";
    
    }  

    public getLines(pageName : String) : Observable<Object> {
        console.log();
        
        return this.http.get(`${this.serverPath}/pages/${pageName}/lines`,this.httpOptions);   
    }

    public get(path) : Observable<Object> {
        return this.http.get(path,this.httpOptions);   
    }

    public pushLine(pageName : String, line : Line) : Observable<Object> {

         let pointsParam : string = "";
         line.points.forEach(p => {
            pointsParam += p.x.toString() + " ";
            pointsParam += p.y.toString() + " ";
         });
        console.log(line.width);
        console.log(line.color);

        return this.http.post(`${this.serverPath}/pages/${pageName}/lines`,null,{
            params : { 
                width : line.width.toString(),
                points : pointsParam,     
                color : line.color,
                
            },
            headers: new HttpHeaders({ 
                'Access-Control-Allow-Origin':'*',
                'Authorization':'authkey',
                'userid':'1'
              })
        })
    }

    public post(path) : Observable<Object> {
        return this.http.post(path,null,this.httpOptions);   
    }
   
    fixedUpdate(): void {
    }
    draw(): void {
    }


}
