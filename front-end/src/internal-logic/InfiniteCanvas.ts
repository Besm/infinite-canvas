import { CanvasSystem  } from "./CanvasSystem";
import { LineDrawerSystem } from './LineDrawerSystem';
import { LineHolderSystem } from "./LineHolderSystem";
import { InputSystem } from './Input';
import { LineRendererSystem } from './LineRendererSystem';
import { CameraSystem } from './CameraSystem';
import { ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestSystem } from './RestSystem';


export class InfiniteCanvas {

    public drawer;
    public pixiApp: PIXI.Application;
    public renderer: PIXI.WebGLRenderer;
    private systems: CanvasSystem[];
    public http : HttpClient;

    constructor(app: PIXI.Application, http : HttpClient) {
        this.pixiApp = app;
        this.http = http;
        this.renderer = app.renderer;
        this.systems =  [];
        this.setupTicker();
        this.setupSystems();
        this.systems.forEach(element => {
            element.start();   

        }); 
   
    }

    setupSystems(): void {
        this.addSystem(RestSystem);
        this.addSystem(LineHolderSystem);
        this.addSystem(LineDrawerSystem);
        this.addSystem(LineRendererSystem);
        this.addSystem(CameraSystem);
        this.addSystem(InputSystem);

    }

    public addSystem<T extends CanvasSystem>(type: (new (inifiniteCanvas : InfiniteCanvas) => T)): T {
       let t = new type(this);
       this.systems.push(t);
       return t;
    }

    public getSystem<T extends CanvasSystem>(type: (new (inifiniteCanvas : InfiniteCanvas) => T)) : any {
        
        for (let i = 0; i < this.systems.length; i++) {
            if(this.systems[i].constructor.name == type.name){
                console.log(this.systems[i].constructor.name + "?=" + type.name);
                return this.systems[i] ;
            }
        }
        throw Error("no system found with name " + type);
    }



    private setupTicker() {
        this.pixiApp.ticker.add((deltatime: 0.1) => this.draw());
        this.pixiApp.ticker.add((deltatime: 0.1) => this.fixedUpdate());
    }

    private fixedUpdate(): void {
        this.systems.forEach(element => {
            element.fixedUpdate();
        });
    }

    private draw(): void {
        this.systems.forEach(element => {
            element.draw();
        });
    }

}
