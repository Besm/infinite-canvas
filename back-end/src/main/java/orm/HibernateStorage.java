package orm;

import models.Line;
import models.Page;
import models.Vector;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HibernateStorage {

    private SessionFactory sessionFactory;

    public HibernateStorage() {

        File hibFile = new File(
                getClass().getClassLoader().getResource("hibernate.cfg.xml").getFile()
        );

        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                .configure(hibFile).build();
        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
        sessionFactory = metadata.getSessionFactoryBuilder().build();


        Page testPage = new Page();
        testPage.setName("test");
        List<Line> lines = new ArrayList<>();
        Line line = new Line();
        line.setColor("blue");
        line.setWidth(5);
        List<Vector> points = new ArrayList<>();
        points.add(new Vector(0, 0));
        points.add(new Vector(100, 0));
        points.add(new Vector(100, 100));
        points.add(new Vector(0, 100));
        points.add(new Vector(0, 0));
        points.add(new Vector(100, 0));

        line.setPoints(points);
        lines.add(line);
        testPage.setLines(lines);

        Session session = setupRequest();
        session.save(testPage);
        finishRequest(session);
    }

    public Session setupRequest() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        return session;
    }

    public void finishRequest(Session session) {
        session.getTransaction().commit();
        session.close();

    }


    public Line getLine(int id) {
        Session session = setupRequest();
        Line line = session.get(Line.class, id);
        if (line == null) {
            return null;
        }
        Hibernate.initialize(line.getPoints());
        finishRequest(session);
        return line.copy();
    }

    public Page getPage(String pageName) {
        Session session = setupRequest();
        Page page = session.get(Page.class, pageName);
        if(page==null){
            page = new Page();
            page.setName(pageName);
            page.setLines(new ArrayList<>());
            Serializable id  = session.save(page);
            page = session.get(Page.class, pageName);
        }
        Hibernate.initialize(page.getLines());
        for (Line line : page.getLines()) {
            Hibernate.initialize(line.getPoints());
        }
        finishRequest(session);
        return page.copy();
    }


    public int addLine(String pageName, Line line) {
        Session session = setupRequest();
        Page page = session.get(Page.class, pageName);
        Hibernate.initialize(page.getLines());
        page.getLines().add(line);
        int id = (int) session.save(line);
        finishRequest(session);
        return id;
    }


    public List<Line> getAllLines() {
        Session session = setupRequest();
        List result = session.createQuery("from Line").list();
        finishRequest(session);
        List<Line> lines = new ArrayList<>();
        for (Object o : result) {
            lines.add((Line) o);
        }
        return lines;
    }
}
