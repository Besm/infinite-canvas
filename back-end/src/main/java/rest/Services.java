package rest;

import orm.HibernateStorage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Services {

    private static List<Class> serviceClasses;
    private static HibernateStorage storage;

    public static void init() {

        storage = new HibernateStorage();
        serviceClasses = new ArrayList<>();
        File dir = new File("src/main/java/rest/services");
        String[] list = dir.list();
        for (String s : list) {
            String name = s.replaceFirst("[.][^.]+$", "");
            try {
                if (s.endsWith("Service.java")) {
                    serviceClasses.add(Class.forName("rest.services." + name));
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    public static String getServicesString() {
        StringBuilder servicesBuilder = new StringBuilder();
        for (Class s : serviceClasses) {
            servicesBuilder.append(s.getCanonicalName());
            servicesBuilder.append(";");
        }
        servicesBuilder.deleteCharAt(servicesBuilder.length() - 1);
        return servicesBuilder.toString();
    }


    public static HibernateStorage getStorage() {
        return storage;
    }
}

