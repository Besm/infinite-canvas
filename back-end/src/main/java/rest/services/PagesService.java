package rest.services;

import models.Line;
import models.Vector;
import rest.ServerResponse;
import rest.Services;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("pages")
public class PagesService {

    @GET
    @Path("{name}")
    public Response getSingle(@PathParam("name") String name){
        return new ServerResponse(true,Services.getStorage().getPage(name)).toResponse();
    }

    @GET
    @Path("{name}/lines")
    public Response getLines(@PathParam("name") String name){
        return new ServerResponse(true,Services.getStorage().getPage(name).getLines()).toResponse();
    }

    @POST
    @Path("{name}/lines")
    public Response getSingle(@PathParam("name") String name,
                              @QueryParam("width") float width,
                              @QueryParam("color") String color,
                              @QueryParam("points") String points){

        List<Vector> vectors = new ArrayList<>();
        String[] s = points.split("\\s+");
        for (int i = 0; i < s.length; i+=2) {

            vectors.add(new Vector(Float.parseFloat(s[i]),Float.parseFloat(s[i+1])));
        }
        Line line =new Line();
        line.setPoints(vectors);
        line.setWidth(width);
        line.setColor(color);

        return new ServerResponse(true,Services.getStorage().addLine(name,line)).toResponse();
    }
}

