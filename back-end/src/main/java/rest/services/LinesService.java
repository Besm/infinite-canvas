package rest.services;

import rest.ServerResponse;
import rest.Services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("lines")
public class LinesService {
    @GET
    @Path("{id}")
    public Response getSingle(@PathParam("id") int id){
        return new ServerResponse(true, Services.getStorage().getLine(id)).toResponse();
    }
}
