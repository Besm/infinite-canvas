package rest;

import com.google.gson.Gson;

import javax.ws.rs.core.Response;

public class ServerResponse {

    private boolean success;
    private Object result;

    public ServerResponse(boolean success, Object result) {
        this.success = success;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public Object getResult() {
        return result;
    }

    private String toJSON() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public Response toResponse() {
        return Response.status(Response.Status.OK).entity(toJSON())
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
}
