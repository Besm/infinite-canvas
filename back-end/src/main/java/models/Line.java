package models;


import java.util.ArrayList;
import java.util.List;

public class Line {

    private int id;
    private String color;
    private float width;
    private List<Vector> points;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }


    public List<Vector> getPoints() {
        return points;
    }

    public void setPoints(List<Vector> points) {
        this.points = points;
    }

    public Line copy(){
        Line l = new Line();
        l.points = new ArrayList<>(points);
        l.color = color;
        l.id = id;
        l.width = width;
        return l;
    }
}
