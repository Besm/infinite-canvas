package models;

public class Vector {

    private float x;
    private float y;
    private int id;

    public Vector(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Vector copy(){
        return new Vector(x,y);
    }
}
