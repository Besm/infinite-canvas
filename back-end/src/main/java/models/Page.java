package models;

import java.util.ArrayList;
import java.util.List;

public class Page {
    private String name;
    private List<Line> lines;

    public Page() {
    }

    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Page copy(){
        Page p = new Page();
        p.name = name;
        p.lines = new ArrayList<>(lines);
        return p;
    }
}
